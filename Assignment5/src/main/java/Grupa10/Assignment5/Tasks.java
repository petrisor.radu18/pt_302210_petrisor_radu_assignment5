package Grupa10.Assignment5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Tasks {
	
	public void completing_task_1(String task,List<MonitoredData> md)
	{
		FileWriterTasks fr=new FileWriterTasks();
		fr.writingInAFile_Task1(task, md);
	}
	public void completing_task_2(String task,List<MonitoredData> md)
	{
		List<Integer> distinct_days=new ArrayList<>();
		for(MonitoredData m:md)
		{
			distinct_days.add(m.getDistinctDayCode());
		}
		long distinct=distinct_days.stream().distinct().count();
		FileWriterTasks fr=new FileWriterTasks();
		fr.writingInAFile_Task2(task, distinct);
	}
	public Map<String,Integer> completing_task_3(String task,List<MonitoredData> md)
	{
		FileWriterTasks fr=new FileWriterTasks();
		List<String> activities=new ArrayList();
		for(MonitoredData m:md)
		{
			activities.add(m.getActivity());
		}
		List<String> distinct_activities=activities.stream().distinct().collect(Collectors.toList());
		long ap=0;
		Map<String,Integer> task3=new HashMap<String,Integer>();
		for(String d:distinct_activities)
		{
		     ap=activities.stream().filter(p->p.equals(d)).count();
			task3.put(d,(int)ap);
		}
		fr.writingInAFile_Task3(task, task3);
		return task3;
	}
	public Map<Integer,Map<String,Integer>> completing_task_4(String task,List<MonitoredData> md)
	{
		Map<Integer,Map<String,Integer>> task4=new HashMap<Integer,Map<String,Integer>>();
		List<Integer> days=new ArrayList<Integer>();
		List<String> activities=new ArrayList<String>();
		for(MonitoredData m:md)
		{
			days.add(m.getDistinctDayCode());
			activities.add(m.getActivity());
		}
		
		List<Integer> distinct_days=days.stream().distinct().collect(Collectors.toList());
		List<String> distinct_activities=activities.stream().distinct().collect(Collectors.toList());
		long nr=0;
		for(Integer day:distinct_days)
		{
			Map <String,Integer> value=new HashMap<String,Integer>();
			for(String s:distinct_activities)
			{
	          nr=md.stream().filter(p->p.getDistinctDayCode()==day.intValue() && p.getActivity().equals(s)).count();
	          value.put(s,new Integer((int)nr));
			}
			task4.put(day,value);
		}
		FileWriterTasks fr=new FileWriterTasks();
		fr.writingInAFile_Task4(task,task4);
		
		return task4;
		
	}
	public Map<String,Integer> completing_task_5(String task,List<MonitoredData> md)
	{
		Map<String,Integer> task5=new HashMap<String,Integer>();
		List<String> activities=new ArrayList<>();
		for(MonitoredData m:md)
		{
			activities.add(m.getActivity());
		}
		List<String> distinct_activities=activities.stream().distinct().collect(Collectors.toList());
		for(String s:distinct_activities)
		{
			float duration=0;
			List<MonitoredData> durations=md.stream().filter(p->p.getActivity().equals(s)).collect(Collectors.toList());
			for(MonitoredData m:durations)
			{
				duration=duration+m.getNumberOfMinutesDifference();
			}
			task5.put(s,(int)duration);
		}
		FileWriterTasks fr=new FileWriterTasks();
		fr.writingInAFile_Task5(task,task5);
		return task5;
	}
	public List<String> completing_task_6(String task,List<MonitoredData> md)
	{
		List<String> task6=new ArrayList<>();
		Tasks tasks=new Tasks();
		Map<String,Integer> task3=tasks.completing_task_3(task, md);
		for(String s:task3.keySet())
		{
			long ap=md.stream().filter(p->p.getActivity().equals(s) && p.getNumberOfMinutesDifference()<5).count();
			float procent=(float)ap/(float)task3.get(s);
			if(procent*100>90)
			{
				task6.add(s);
			}
		}
		FileWriterTasks fr=new FileWriterTasks();
		fr.writingInAFile_Task6(task,task6);
		return task6;
	}
}
