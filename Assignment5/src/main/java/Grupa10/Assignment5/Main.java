package Grupa10.Assignment5;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		List<MonitoredData> md=new ArrayList<MonitoredData>();
		FileReader reader=new FileReader();
		FileWriterTasks writer=new FileWriterTasks();
		Tasks tasks=new Tasks();
		md=reader.read();
		tasks.completing_task_1("Task_1.TXT", md);
		tasks.completing_task_2("Task_2.TXT", md);
		tasks.completing_task_3("Task_3.TXT", md);
		tasks.completing_task_4("Task_4.TXT", md);
		tasks.completing_task_5("Task_5.TXT", md);
		tasks.completing_task_6("Task_6.TXT", md);
	}

}
