package Grupa10.Assignment5;

import java.security.Timestamp;
import java.time.Duration;
import java.util.Date;

public class MonitoredData {
	private Date start_time;
	private Date end_time;
	private String activity;
	
	public MonitoredData(Date start,Date end,String activity)
	{
		this.start_time=start;
		this.end_time=end;
		this.activity=activity;
	}
	public void setStartTime(Date start)
	{
		this.start_time=start;
	}
	public void setEndTime(Date end)
	{
		this.end_time=end;
	}
	public void setActivity(String a)
	{
		this.activity=a;
	}
	public Date getStartTime()
	{
		return this.start_time;
	}
	public Date getEndTime()
	{
		return this.end_time;
	}
	public String getActivity()
	{
		return this.activity;
	}
	public float getNumberOfMinutesDifference()
	{
		float time1,time2;
		if(this.start_time.getDate()==this.end_time.getDate())
		{
		time1=this.start_time.getHours()*60+this.start_time.getMinutes()+(float)this.start_time.getSeconds()/60;
		time2=this.end_time.getHours()*60+this.end_time.getMinutes()+(float)this.end_time.getSeconds()/60;
		}
		else
		{
		time1=this.start_time.getHours()*60+this.start_time.getMinutes()+(float)this.start_time.getSeconds()/60;
		time2=(24+this.end_time.getHours())*60+this.end_time.getMinutes()+(float)this.end_time.getSeconds()/60;
		}
		return (time2-time1);
	}
	public int getDistinctDayCode()
	{
		return (this.start_time.getMonth()+1)*31+this.start_time.getDate();
	}
	public String toString()
	{
		return "start_time:"+this.start_time.toString()+" end_time:"+this.end_time.toString()+" activity:"+this.activity;
	}
}
