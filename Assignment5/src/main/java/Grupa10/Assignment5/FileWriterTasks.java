package Grupa10.Assignment5;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FileWriterTasks {
	
	public void writingInAFile_Task1(String file,List<MonitoredData> md)
	{
		try {
			FileWriter fr=new FileWriter(file);
			for(MonitoredData m:md)
			{
			   fr.write(m.toString());
			   fr.write("\n");
			}
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public void writingInAFile_Task2(String file,long d)
	{
		try {
			FileWriter fr=new FileWriter(file);
			fr.write("Number of distinct days: ");
			fr.write(String.valueOf(d));
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public void writingInAFile_Task3(String file, Map<String,Integer> hm)
	{
		try {
			FileWriter fr=new FileWriter(file);
            for(String s:hm.keySet())		
            {
            	fr.write("Activity:"+s+" Number of aparitions:"+hm.get(s)+"\n");
            }
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public void writingInAFile_Task4(String file, Map<Integer,Map<String,Integer>> hm)
	{
		try {
			FileWriter fr=new FileWriter(file);
			for(Integer i:hm.keySet())
			{
				fr.write("Activities for day "+i.intValue()%31+"/"+i.intValue()/31+"/2011:\n\n");
            for(String s:hm.get(i).keySet())		
            {
            	fr.write("Activity:"+s+" Number of aparitions:"+hm.get(i).get(s)+"\n");
            }
                fr.write("\n");
			}
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public void writingInAFile_Task5(String file, Map<String,Integer> hm)
	{
		try {
			FileWriter fr=new FileWriter(file);
			for(String s:hm.keySet())
			{
				fr.write("Activity:"+s+" Number of minutes over the monitoring period:"+hm.get(s)+"\n");
			}
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	public void writingInAFile_Task6(String file, List<String> list)
	{
		try {
			FileWriter fr=new FileWriter(file);
			fr.write("Activities with 90 procent aparitions less than 5 minutes:\n");
			for(String s:list)
			{
				fr.write(s+"\n");
			}
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	

}
