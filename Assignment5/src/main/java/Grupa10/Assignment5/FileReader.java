package Grupa10.Assignment5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReader {
	
    public List<MonitoredData> read()
    {
    	List<MonitoredData> md=new ArrayList<MonitoredData>();
    	List<Object> streams=new ArrayList<>();
    	String fileName="Activities.TXT";
    	try(Stream<String> stream=Files.lines(Paths.get(fileName)))
    	{
    		 streams=stream.collect(Collectors.toList());
    	}catch(IOException e)
    	{
    		e.printStackTrace();
    	}
    	
    	String delimitator="[\\t]+";
    	String []tokens;
    	
    	for(Object lines:streams)
    	{
    		String line=(String)lines;
    		tokens=line.split(delimitator);
    		String delimitator1="[-]";
 	        String delimitator2="[ ]";
 	        String delimitator3="[:]";
 	        String []tokensstart=tokens[0].split(delimitator2);
 	        String []tokensend=tokens[1].split(delimitator2);
 	        String []tokensstarttime=tokensstart[1].split(delimitator3);
 	        String []tokensstartdate=tokensstart[0].split(delimitator1);
 	        String []tokensendtime=tokensend[1].split(delimitator3);
 	        String []tokensenddate=tokensend[0].split(delimitator1);
 	        Date start=new Date(Integer.valueOf(tokensstartdate[0].trim())-1900,
 	        		Integer.valueOf(tokensstartdate[1].trim())-1,
 	        		Integer.valueOf(tokensstartdate[2].trim()),
 	                Integer.valueOf(tokensstarttime[0].trim()),
 	                Integer.valueOf(tokensstarttime[1].trim()),
 	                Integer.valueOf(tokensstarttime[2].trim()));
 	        Date end=new Date(Integer.valueOf(tokensenddate[0].trim())-1900,
 	        		Integer.valueOf(tokensenddate[1].trim())-1,
 	        		Integer.valueOf(tokensenddate[2].trim()),
 	    	        Integer.valueOf(tokensendtime[0].trim()),
 	    	        Integer.valueOf(tokensendtime[1].trim()),
 	    	        Integer.valueOf(tokensendtime[2].trim()));
 	        MonitoredData m=new MonitoredData(start,end,tokens[2]);
 	        md.add(m);
    	}
    	return md;
    	
    }
}
